/** @type {import('tailwindcss').Config} */
export default {
  content: ['./src/**/*.{html,js,svelte,ts}'],
  theme: {
    extend: {
      colors: {
        'tgdp-disco': '#981C50',
        'tgdp-red-violet': '#CD1587',
        'tgdp-flamingo': '#F15A2B',
        'tgdp-sun': '#FAA71D',
        'tgdp-candlelight': '#FBCE0F',
        'tgdp-sunflower': '#E0E326',
        'tgdp-atlantis': '#AAD03C',
        'tgdp-eastern-blue': '#18ADA7',
        'tgdp-blue': '#262262'
      },
    },
    aspectRatio: {
      auto: 'auto',
      square: '1 / 1',
      video: '16 / 9',
      1: '1',
      2: '2',
      3: '3',
      4: '4',
      5: '5',
      6: '6',
      7: '7',
      8: '8',
      9: '9',
      10: '10',
      11: '11',
      12: '12',
      13: '13',
      14: '14',
      15: '15',
      16: '16',
    }
  },
  safelist: [
    'text-tgdp-disco',
    'text-tgdp-red-violet',
    'text-tgdp-flamingo',
    'text-tgdp-sun',
    'text-tgdp-candlelight',
    'text-tgdp-sunflower',
    'text-tgdp-atlantis',
    'text-tgdp-eastern-blue',
    'text-tgdp-blue',
    'bg-tgdp-disco',
    'bg-tgdp-red-violet',
    'bg-tgdp-flamingo',
    'bg-tgdp-sun',
    'bg-tgdp-candlelight',
    'bg-tgdp-sunflower',
    'bg-tgdp-atlantis',
    'bg-tgdp-eastern-blue',
    'bg-tgdp-blue',
    'underline'
  ],
  plugins: [
    require('@tailwindcss/aspect-ratio'),
    require('@tailwindcss/typography'),
    require('@tailwindcss/forms')
    ],
}
