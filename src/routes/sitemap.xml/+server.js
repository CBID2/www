//sitemap.xml.js
import {blogs} from '$lib/dataBlog'
import {authors} from '$lib/dataAuthor'
import {tactics} from '$lib/dataTactic'
import {pages} from '$lib/dataPage'

export const prerender = true;

/** @type {import('./$types').RequestHandler} */
export async function GET() {
  const body = sitemap(blogs, authors, tactics, pages)
  const headers = {
    'Cache-Control': 'max-age=0, s-maxage=3600',
    'Content-Type': 'application/xml',
  }

  return new Response(
    body,
    headers
  );;
}

const sitemap = (
  blogs, authors, tactics, pages
) => `<?xml version="1.0" encoding="UTF-8" ?>
<urlset
  xmlns="https://www.sitemaps.org/schemas/sitemap/0.9"
  xmlns:news="https://www.google.com/schemas/sitemap-news/0.9"
  xmlns:xhtml="https://www.w3.org/1999/xhtml"
  xmlns:mobile="https://www.google.com/schemas/sitemap-mobile/1.0"
  xmlns:image="https://www.google.com/schemas/sitemap-image/1.1"
  xmlns:video="https://www.google.com/schemas/sitemap-video/1.1"
>
  <url>
    <loc>https://www.thegooddocsproject.dev/</loc>
    <changefreq>monthly</changefreq>
    <priority>0.7</priority>
  </url>
  ${pages
    .filter(p => p.slug != "home")
    .map(page => `
  <url>
    <loc>https://www.thegooddocsproject.dev/${page.slug}/</loc>
    <changefreq>monthly</changefreq>
    <priority>0.7</priority>
  </url>
  `
    )
    .join('')}
  ${blogs.posts
    .map(post => `
  <url>
    <loc>https://www.thegooddocsproject.dev/blog/${post.slug}/</loc>
    <changefreq>yearly</changefreq>
    <priority>0.7</priority>
  </url>
  `
    )
    .join('')}
    ${authors
    .map(author => `
  <url>
    <loc>https://www.thegooddocsproject.dev/author/${author.slug}/</loc>
    <changefreq>yearly</changefreq>
    <priority>0.7</priority>
  </url>
  `
    )
    .join('')}
    ${tactics
    .map(tactic => `
  <url>
    <loc>https://www.thegooddocsproject.dev/tactic/${tactic.slug}/</loc>
    <changefreq>yearly</changefreq>
    <priority>0.7</priority>
  </url>
  `
    )
    .join('')}
</urlset>`