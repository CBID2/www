import { error } from '@sveltejs/kit'
import Processor from '@asciidoctor/core'
import { parse } from 'yaml'

const processor = Processor()

export const load = (async ({ params }) => {
  let adocFile

  try {
    adocFile = await import(`../../content/${params.slug}.adoc?raw`)
  } catch (err) {
    error(404, 'Not found');
  }

  let attributes = processor.load(adocFile.default, {'standalone':false, 'safe':'unsafe', 'attributes':{'icons':'font', 'allow-uri-read':'', 'sectanchors':true, 'skip-front-matter':true}}).getAttributes()
  let html = processor.convert(adocFile.default, {'standalone':false, 'safe':'unsafe', 'attributes':{'icons':'font', 'allow-uri-read':'', 'sectanchors':true, 'skip-front-matter':true, 'imagesdir':'https://tgdp-assets.imgix.net'}})
  let frontmatter = parse(attributes['front-matter'])
  
  return {
    html,
    attributes,
    frontmatter
  }
});