import { error } from '@sveltejs/kit'
import { blogs } from '$lib/dataBlog'

export const load = (async ({ params }) => {
  const postData = blogs.posts.filter((post)=> post.tags.includes(params.slug))

  if (postData.length > 0) {
    return {
      posts: postData,
      tag: params.slug
    }
  } else {
    error(404, 'Posts were not found using this tag.');
  }
});