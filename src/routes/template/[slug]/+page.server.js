import { error } from '@sveltejs/kit'
import { templates } from '$lib/dataTemplate'

export const load = (async ({ params }) => {
  const templateData = templates.find((template)=>template.name.split("/")[1].split(".")[0] == params.slug)

  if (templateData) {
    return {
      template: templateData
    }
  } else {
    error(404, 'Template Guide not found.');
  }
});