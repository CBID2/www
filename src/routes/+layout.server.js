import { authors } from "$lib/dataAuthor";
import { blogs } from "$lib/dataBlog";
import { tactics } from "$lib/dataTactic";
import { templates } from "$lib/dataTemplate";

export const prerender = true;

export const load = async () => {
  return {
    authors,
    blogs,
    tactics,
    templates
  }
};