import { error } from '@sveltejs/kit'
import Processor from '@asciidoctor/core'
import { parse } from 'yaml'

const processor = Processor()

export const load = (async () => {
  const adocFile = await import(`../content/home.adoc?raw`)  

  if (adocFile) {
    let attributes = processor.load(adocFile.default, {'standalone':false, 'safe':'unsafe', 'attributes':{'icons':'font', 'allow-uri-read':'', 'sectanchors':true, 'skip-front-matter':true}}).getAttributes()
    let html = processor.convert(adocFile.default, {'standalone':false, 'safe':'unsafe', 'attributes':{'icons':'font', 'allow-uri-read':'', 'sectanchors':true, 'skip-front-matter':true, 'imagesdir':'https://tgdp-assets.imgix.net'}})
    let frontmatter = parse(attributes['front-matter'])
    return {
      html,
      attributes,
      frontmatter
    }
  }
  error(404, 'Not found');
});