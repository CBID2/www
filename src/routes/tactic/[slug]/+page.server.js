import { error } from "@sveltejs/kit";
import { tactics } from '$lib/dataTactic'

export async function load({params}) {
  const tactic_data = tactics.find((tactic)=>tactic.slug == params.slug);
  
  if (tactic_data) {
    return {
      article: tactic_data,
      slug: params.slug
    }
  } else {
    error(404, "A Tactic Article was not found for that URL.")
  }
}