import Processor from '@asciidoctor/core'
import { parse } from 'yaml'
import { compare_post_count } from './utils';
import { blogs } from '$lib/dataBlog';

const processor = Processor()
const allAuthorFiles = import.meta.glob("../content/author/**/*.adoc");
const iterableAuthorFiles = Object.entries(allAuthorFiles);
let allAuthorPages = [];

allAuthorPages = await Promise.all(
  iterableAuthorFiles.map(async ([path]) => {
    const adocFile = await import(`../content/author/${path.slice(18,-5)}.adoc?raw`)
    const attributes = processor.load(adocFile.default, {'standalone':false, 'safe':'unsafe', 'attributes':{'icons':'font', 'allow-uri-read':'', 'sectanchors':true, 'skip-front-matter':true }}).getAttributes()
    const html = processor.convert(adocFile.default, {'standalone':false, 'safe':'unsafe', 'attributes':{'icons':'font', 'allow-uri-read':'', 'sectanchors':true, 'skip-front-matter':true, 'imagesdir':'https://tgdp-assets.imgix.net'}})
    const frontmatter = parse(attributes['front-matter'])
    const authorPath = path.slice(1, -5);
    
    let posts = []

    if (path.slice(18,-5)){
      posts = blogs.posts.filter((post)=> post.author.includes(path.slice(18,-5)))
    }

    return { ... frontmatter,
      path: authorPath,
      slug: authorPath?.substring(17),
      posts,
      postcount: posts.length,
      html,
      type: "adoc"
    };
  })
);

export const authors = allAuthorPages.sort(compare_post_count)
