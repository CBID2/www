import Processor from '@asciidoctor/core'
import { parse } from 'yaml'
import { compare_name, count_words } from './utils';
import { convert } from 'html-to-text'

const processor = Processor()
const allTacticFiles = import.meta.glob("../content/tactic/**/*.adoc");
const iterableTacticFiles = Object.entries(allTacticFiles);
let allTacticPosts = [];

allTacticPosts = await Promise.all(
  iterableTacticFiles.map(async ([path]) => {
    const adocFile = await import(`../content/tactic/${path.slice(18,-5)}.adoc?raw`)
    const attributes = processor.load(adocFile.default, {'standalone':false, 'safe':'unsafe', 'attributes':{'icons':'font', 'allow-uri-read':'', 'sectanchors':true, 'skip-front-matter':true}}).getAttributes()
    const frontmatter = attributes?parse(attributes['front-matter']):{};
    const postPath = path.slice(18,-5);
    const postDate = frontmatter.date?new Date(frontmatter.date).toLocaleString("en-US", {timeZone:'UTC', year:'numeric', month:'short', day:'2-digit'}):undefined
    const postLastMod = frontmatter.lastmod?new Date(frontmatter.lastmod).toLocaleString("en-US", {timeZone:'UTC', year:'numeric', month:'short', day:'2-digit'}):undefined
    const postImage = frontmatter.image?frontmatter.image:undefined
    const postTitle = frontmatter?frontmatter.title:attributes.doctitle
    const postTags = frontmatter?frontmatter.tags:[]

    let authors = []

    if (frontmatter.author){
      for (let index = 0; index < frontmatter.author.length; index++) {
        const authorFile = await import(`../content/author/${frontmatter.author[index]}.adoc?raw`)
        const attributes = processor.load(authorFile.default, {'standalone':false, 'safe':'unsafe', 'attributes':{'icons':'font', 'allow-uri-read':'', 'sectanchors':true, 'skip-front-matter':true}}).getAttributes()
        const fm = parse(attributes['front-matter']);
        const authorInfo = {
          "name": fm.name,
          "id": frontmatter.author[index],
          "description": fm.description,
          "follow": fm.follow,
          "image": fm.image
        }
        authors.push(authorInfo)
      }
    }

    const html = processor.convert(adocFile.default, {'standalone':false, 'safe':'unsafe', 'attributes':{'icons':'font', 'allow-uri-read':'', 'sectanchors':true, 'skip-front-matter':true, 'imagesdir':'https://tgdp-assets.imgix.net'}})
    const plaintext = convert(html, {
      baseElements: {
        orderBy: "occurrence",
      },
      wordwrap: false,
      selectors: [
        { selector: "h1", format: "skip" },
        { selector: "h2", options: { uppercase: false }},
        { selector: "a", options: { ignoreHref: true }},
        { selector: "a.button", format: "skip" },
        { selector: "a.btn", format: "skip" },
        { selector: "aside", format: "skip" },
        { selector: "ul", options: { itemPrefix: "- " }},
        { selector: "img", format: "skip" },
      ],
    })
    .replace(/\n\n\n/gm, " ")
    .replace(/\n\n/g, " ")
    .replace(/\n/g, " ")
    .replace(/\"/g, "'")
    .trim();
    
    return { ... frontmatter,
      name: postTitle,
      id: postPath,
      date: postDate,
      lastmod: postLastMod,
      path: postPath,
      image: postImage,
      slug: postPath,
      tags: postTags,
      authors,
      html: html,
      wordcount: count_words(plaintext),
      type: "adoc"
    };
  })
);

export const tactics = allTacticPosts.sort(compare_name)