import { Gitlab } from '@gitbeaker/rest';
import { GITLAB_TOKEN } from '$env/static/private';
import { marked } from 'marked';
import { compare_name } from './utils';

const api = new Gitlab({
  token: GITLAB_TOKEN,
});

let allTemplateFiles = await api.Repositories.allRepositoryTrees(39683789, {recursive: true});
let allTemplates = [];

function decodeBase64(base64) {
  const text = atob(base64);
  const length = text.length;
  const bytes = new Uint8Array(length);
  for (let i = 0; i < length; i++) {
      bytes[i] = text.charCodeAt(i);
  }
  const decoder = new TextDecoder(); // default is utf-8
  return decoder.decode(bytes);
}

for (let index = 0; index < allTemplateFiles.length; index++) {
  
  const file = allTemplateFiles[index];

  if (file.path.search('guide-') > 0) {
    const uri = encodeURI(file.path)
    const fileData = await api.RepositoryFiles.show(39683789, uri, "main");
    const fileContent = decodeBase64(fileData.content)
    const html = marked.parse(fileContent)
    allTemplates.push(
      {
        id: file.id,
        name: file.path,
        slug: file.path.split("/")[1].split(".")[0],
        html
      }
    )
  }
}

export const templates = allTemplates.sort(compare_name)