import Processor from '@asciidoctor/core'
import { parse } from 'yaml'
import { compare_title, count_words } from './utils';
import { convert } from 'html-to-text'

const processor = Processor()
const allPageFiles = import.meta.glob("../content/*.adoc");
const iterablePageFiles = Object.entries(allPageFiles);
let allPages = [];

allPages = await Promise.all(
  iterablePageFiles.map(async ([path]) => {
    const adocFile = await import(`../content/${path.slice(11,-5)}.adoc?raw`)
    const attributes = processor.load(adocFile.default, {'standalone':false, 'safe':'unsafe', 'attributes':{'icons':'font', 'allow-uri-read':'', 'sectanchors':true, 'skip-front-matter':true, 'imagesdir':'https://tgdp-assets.imgix.net' }}).getAttributes()
    const frontmatter = attributes?parse(attributes['front-matter']):{};
    const pagePath = path.slice(0, -5);
    const pageDate = frontmatter.date?new Date(frontmatter.date).toLocaleString("en-US", {timeZone:'UTC', year:'numeric', month:'short', day:'2-digit'}):undefined
    const pageLastMod = frontmatter.lastmod?new Date(frontmatter.lastmod).toLocaleString("en-US", {timeZone:'UTC', year:'numeric', month:'short', day:'2-digit'}):undefined
    const pageImage = frontmatter.image?frontmatter.image:undefined

    const html = processor.convert(adocFile.default, {'standalone':false, 'safe':'unsafe', 'attributes':{'icons':'font', 'allow-uri-read':'', 'sectanchors':true, 'skip-front-matter':true, 'imagesdir':'https://tgdp-assets.imgix.net'}})
    const plaintext = convert(html, {
      baseElements: {
        orderBy: "occurrence",
      },
      wordwrap: false,
      selectors: [
        { selector: "h1", format: "skip" },
        { selector: "h2", options: { uppercase: false }},
        { selector: "a", options: { ignoreHref: true }},
        { selector: "a.button", format: "skip" },
        { selector: "a.btn", format: "skip" },
        { selector: "aside", format: "skip" },
        { selector: "ul", options: { itemPrefix: "- " }},
        { selector: "img", format: "skip" },
      ],
    })
    .replace(/\n\n\n/gm, " ")
    .replace(/\n\n/g, " ")
    .replace(/\n/g, " ")
    .replace(/\"/g, "'")
    .trim();
    
    return { ... frontmatter,
      date: pageDate,
      lastmod: pageLastMod,
      path: pagePath,
      image: pageImage,
      slug: pagePath.substring(11),
      html: html,
      plaintext,
      wordcount: count_words(plaintext),
      type: "adoc"
    };
  })
);

export const pages = allPages.sort(compare_title)