---
draft: false
title: Becoming a Keystone Project
date: 2023-05-15
lastmod: 2023-05-23T07:31:23.243Z
image: keystone.png
author:
  - cameron_shorter
tags:
  - the-project
custom_copyright: ""
custom_license: ""
summary: The Good Docs Project is growing into a keystone project for software ecosystems. Read on to learn about what a keystone project is and why they matter.
---
https://thegooddocsproject.dev[The Good Docs Project] is growing into a _keystone project_ for software ecosystems.

## What's a Keystone Project?

In architecture, a masonry arch cannot be self-supporting until the https://en.wikipedia.org/wiki/Keystone_(architecture)[keystone] is placed. This stone locks together the whole structure.

The concept is also found in ecology. A https://education.nationalgeographic.org/resource/role-keystone-species-ecosystem/[keystone species] is an organism that helps define an entire ecosystem - and it has a disproportionate influence on the ecosystem around it. Bees are a _keystone species_. Without bees, flowers don't get pollinated, plants don't reproduce, animals starve, and the ecosystem collapses.

In the technology domain, https://git-scm.com/[Git] can be thought of as a _keystone project_. Its version control underpins many software projects, meaning it has a disproportionate positive impact on the software ecosystem.

## Is documentation disproportionately impactful?

Yes. Time and again, surveys call out documentation quality as a key criteria to:

* Ensure developer productivity,
* Ensure product quality, and
* Attract a user base.

For instance:

____
Developers see about a 50% productivity boost when documentation is up-to-date, detailed, reliable, and comes in different formats.-https://octoverse.github.com/2021/creating-documentation/[The 2021 State of the Octoverse&#44; Github]
____

… Find more stats in the https://docs.google.com/presentation/d/1fZqNm7WH1hYgmjuq2lkltbfTZYDZeclpuaoLabjihJ8/[Docs Fact Pack].

## What is good documentation?

Good documentation provides:

____
Just enough info, When it is needed, To support a specific action, At the quality required.
____

Getting this balance right is both an art and a science. The Good Docs Project explains how, by providing best practice templates and writing instructions for documenting open source software.

## Are we there yet?

Not quite. We will be a _keystone project_ when: 

* Open source surveys stop https://docs.google.com/presentation/d/1fZqNm7WH1hYgmjuq2lkltbfTZYDZeclpuaoLabjihJ8/edit#slide=id.gfc7ec19147_1_38[reporting poor documentation] as a key developer gripe, and 
* People attribute documentation improvements to https://thegooddocsproject.dev/[The Good Docs Project] templates and processes.

'''''

*Image credits:* Keystone image used with permission from Hakai Magazine, _https://hakaimagazine.com/videos-visuals/how-ecosystems-got-keystone/[How Ecosystems Got a Keystone]_, Jude Isabella and Adrienne Mason, 2015.
