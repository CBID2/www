---
title: Our Team
layout: blocks
blocks:
  - block_ref: 'content'
    title: Our Team
---

This page lists the people who currently contribute in key roles in the
Good Docs Project and what their responsibilities are.

The Good Docs Project is powered by a great community and a team of
dedicated folks who keep our project moving forward. We are always
looking for more people to help with:

* Running and participating in working groups.
* Onboarding and welcoming new community members.
* Assisting with the project's IT needs.

If you're interested in helping out, get in touch with someone from the
team you're interested in. The best way to contact these team members is
on our Slack workspace, which you can get invited to by attending one of
our link:/join-us[Welcome Wagon] meetings.

== List of teams

* link:#_project_steering_committee[Project steering committee]
* link:#_community_managers[Community managers]
* link:#_tech_team[Tech team]
* link:#_template_product_management_team[Template product management team]
* link:#_working_group_leads[Working group leads]
* link:#_thank_you_to_past_team_members[Thank you to past team members]

== Project steering committee

The Project Steering Committee (PSC) is the official managing body for
our project.

This team:

* Has a duty to define and uphold the project's mission, goals, and
plans.
* Nominates and votes to approve individuals to key community roles and
teams.
* Discusses and votes on project business and major project initiatives,
such as requests-for-comment (RFCs).
* Is chaired by 1-3 individuals, who act as tie-breaking votes and who
help ensure the efficiency and effectiveness of the PSC.

The current project steering committee members are:

* Michael Park, co-chair
* Tina Lüdtke, co-chair
* Ailine Dominey
* Alyssa Rock
* Ane Tröger
* Bolaji Ayodeji
* Bryan Klein
* Cameron Shorter
* Carrie Crowe
* Deanna Thompson
* Erin McKean
* Gayathri Krishnaswamy
* Lana Novikova
* Mengmeng Tang
* Michael Vallance
* Valeria Hernandez

The committee is drawn from members of the community and is based on merit and interest.

The project steering committee:

* Aims to attract a diverse mix of personal characteristics, reflective of the community we represent.
* Avoids having employees of any one organization representing a controlling proportion of the PSC.
* May retire any time. If a PSC member becomes inactive for multiple quarters, it may be time to retire.
(Ex-members will typically be welcomed back if they become active again.) 
An inactive member may be invited to retire.
If unresponsive, they may be removed by the existing PSC.

If you are interested in serving on the PSC, let a current member of the PSC know about your interest.

== Community managers

This community managers ensure our community is healthy, vibrant, and safe for all who want to participate.

This team is responsible for:

* Fostering a positive community culture.
* Growing our community.
* Onboarding, mentoring, and training community members.
* Ensuring our community is safe and supported by:
** Handling link:/code-of-conduct[Code of Conduct] incidents.
** Moderating our community forums.

The current community managers are:

* Alyssa Rock
* Carrie Crowe
* Cat Keller
* Deanna Thompson

If you are interested in serving as a community manager, let one of the community managers know.
You can also join the bi-weekly community onboarding and training sync to start volunteering to assist the community managers.

If you experience an incident which makes you feel less safe in our community or less able to be your authentic self, contact the community manager with whom you have the most comfortable relationship and let them know.

The best way to contact these team members is on our Slack workspace in the *#ask-a-community-manager* channel or through a private direct message.
You can get invited to our Slack workspace by attending one of our link:/join-us[Welcome Wagon] meetings.

For more information, see:

* link:/code-of-conduct[The Good Docs Project Code of Conduct]

== Tech team

The Tech team helps with the project’s IT services and tooling needs, including repository access.

This team:

* Maintains access to all the project's IT services and tools.
* Enforces and maintains our security policies.
* Consults on IT related requests and needs.

The current Tech team members are:

* Alyssa Rock
* Bolaji Ayodeji
* Bryan Klein
* Michael Park

If you are interested in joining the Tech team, let one of the members of this team know.
You can also join the bi-weekly DocOps Registry working group, which acts as an entry point for this team.

The best way to contact the team is on our Slack workspace in the *#tech-requests* channel.
You can get invited to our Slack workspace by attending one of our link:/join-us[Welcome Wagon] meetings.

== Template product management team

The template product management team are responsible for defining the vision of the template suite product and related value streams.

This team:

* Creates the template product roadmap.
* Conducts user research and gathers feedback to determine what initiatives should go into the roadmap.
* Coordinates with the template working group to put the roadmap into action (by creating issues to track work, defining priorities, and more).
* Ensures our templates have a consistent style and voice.

The current template product team members are:

* Tina Lüdtke (lead)
* Alyssa Rock (support)
* Ane Troger (style guide lead)

If you are interested in joining the template product management team, let one of the members of this team know.

The best way to contact the team is on our Slack workspace in the *#project-roadmap* channel.
You can get invited to our Slack workspace by attending one of our link:/join-us[Welcome Wagon] meetings.

== Template editorial team

The template editorial team reviews templates when they are nearing completion to ensure the template:

* Follows best practices for technical writing.
* Has no major organization or structural issues.
* Has no obvious gaps or missing content.
* Is consistent with our style guide.

This team includes at least one member from each template working group who are both experienced technical writers and experienced in the template contributing process.
They are also experts on our project's style guide and parent style guides.
They meet at least once a release cycle (or as needed) to review style questions and make decisions, led by our lead style guide editor.
They also assist in socializing the style guide with their working group.

The current template editorial team members are:

* Ane Troger (style guide lead, Team Alpaca)
* Alyssa Rock (Team Dolphin)
* Cameron Shorter (Team Macaw)
* Michael Park (Team Macaw)

== Working group leads

The working group leads help manage our project's various working groups.
They typically chair working group meetings and act as light project managers that keep GitLab issues up to date and communicate the status of open issues to the project maintainers.
They help working group members when they are blocked on tasks and coordinate reviews or other resources as needed.

The current working group leads for each working group are:

[options="header"]
|===
|Working group |Lead(s)
|Team Alpaca Templates and Chronologue (US/APAC)
a|
* Ailine Dominey (templates)
* Deanna Thompson (templates)
* Michael Park (Chronologue)
|Team Macaw Templates and Chronologue (APAC/EMEA)
a|
* Michael Park (Chronologue)
* Michael Vallance (templates)
|Team Dolphin Templates and Chronologue (US/EMEA)
a|
* Alyssa Rock (templates)
* Michael Hungbo (Chronologue)
|DocOps registry
a|
* Bryan Klein
* Michael Park
|UX and outreach
a|
* Carrie Crowe (outreach)
* Tina Lüdtke (UX)
|===

To join any of these working groups, see the link:/community[Community Calendar] for meeting times.

== Thank you to past team members

We are thankful for the hard work and dedication of these past team members:

Previous PSC members:

* Aaron Peters
* Aidan Doherty
* Ankita Tripathi
* Becky Todd
* Clarence Cromwell
* Felicity Brand
* Jared Morgan
* Jennifer Rondeau
* Jo Cook
* Morgan Craft
* Nelson Guya
* Ryan Macklin
* Viraji Ogodapola
