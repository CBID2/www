# The Good Docs Project Website

## Welcome

This project is the source of The Good Docs Project website at [www.thegooddocsproject.dev](https://www.thegooddocsproject.dev).

The Good Docs Project is a community of people who care about good documentation.
We are a group of volunteers who want to help improve documentation by providing a set of templates and tools that can be used by developers, technical writers, product owners, and other stakeholders.

## Table of contents

[Who is this project for?](#who-this-project-is-for)  

[Tech Stack](#tech-stack)  

[Development](#development)  
[Contributing Guidelines](#contributing-guidelines)

## Who this project is for

This project is for members of The Good Docs Project to present information about the project, related information about the templates we create, and the work we do to a global audience.
If you're a frontend developer, full stack developer, or a technical writer who wants to develop their coding skills, check out our Contributing Guide!
We're excited to see your ideas!

## Tech Stack

* [GitLab](https://gitlab.com/): Source Control and Project Management.
* [SvelteKit](https://kit.svelte.dev/): Site Framework.
* [Svelte](https://svelte.dev/): Templating Language.
* [AsciiDoc](https://docs.asciidoctor.org/asciidoc/latest/): Format of content source files (instead of Markdown).
* [AsciiDoctor.js](https://docs.asciidoctor.org/asciidoctor.js/latest/): Tool to transform content source into HTML.
* [Gitbeaker](https://github.com/jdalrymple/gitbeaker#readme): GitLab SDK used for Template Guide content.
* [Marked](https://marked.js.org/): Tool to transform template guide markdown to HTML.
* [Netlify](https://www.netlify.com/): CI/CD and site Hosting.
* Misc: [Various NPM packages](https://gitlab.com/tgdp/www/-/blob/main/package.json?ref_type=heads)

## Quickstart setup

1. Place a request to be a member of the repository in the #tech-request channel in our Slack.
2. Follow a path below in the [How to Edit](#how-to-edit) section.

## How to Edit

Expand the section below based on how you would like to contribute changes to the project.

<details><summary>Edit Online with Gitpod</summary>
<p>

Learn more with the [Gitpod Docs](https://www.gitpod.io/docs)

### Create a Free Gitpod Account

You only have to do this step the first time, to create a Gitpod account, after that you can just click the buttons in the GitLab UI.
For more information on setting up a free account, see <https://gitpod.io/plans>

NOTE: Authenticate with the GitLab account you use in the Good Docs Project.
  
### Install the GitPod Browser Extension

The [Browser Extension](https://www.gitpod.io/docs/browser-extension) will add GitPod buttons to the GitLab UI to make it easier to launch GitPod from appropriate contexts in the project.

### Click the Gitpod Button

This should launch Gitpod and a web based editor with live preview.

NOTE: The [context](https://www.gitpod.io/docs/context-urls) in which you click the button, will change the GitPod environment and remote connections for you.

### Edit and Commit

This works a lot like how tools like VS Code work, where you stage your changes and commit to the branch and submit a Merge Request (MR) to the `main` branch of the project.
Only with GitPod when you are done, you just close the browser and nothing is left behind on your computer.

There is a YouTube video made in the Context of the 'Chronologue' project, that provides and example of this workflow and using GitPod to make changes to a site.

<https://www.youtube.com/watch?v=TT-Znc5kNCc>

The process is the same in this project, though the specific details are a bit different.

</p>
</details>

<details><summary>Edit Online with GitLab Editors</summary>
<p>

This option is typically used for quick changes, or when a full preview of your work on the site is unnecessary.

You have 2 primary options in GitLab:

1. [GitLab Web Editor](https://docs.gitlab.com/ee/user/project/repository/web_editor.html)
1. [GitLab Web IDE](https://docs.gitlab.com/ee/user/project/web_ide/)

NOTE: It is best practice to start with an issue, and create a branch and Merge Request for that branch.
Then, open the editor with that branch selected in the GitPod UI.

</p>
</details>

## Contributing guidelines

All contributors are required to abide by our [Code of Conduct](CODE_OF_CONDUCT.md).
Please follow our [Contributing Guidelines](CONTRIBUTING.md) to contribute to the project.

## Our Contributors
<!-- Add some sort of GitLab Action that displays a list of the contributors to the project. -->
Thank you so much for your time, effort, and dedication in contributing to the website! 😄
  Without your contributions, this project wouldn't have been possible.

---

> Explore other templates from [The Good Docs Project](https://thegooddocsproject.dev/). Use our [feedback form](https://thegooddocsproject.dev/feedback/?template=Readme) to give feedback on this template.
