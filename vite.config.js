import { sveltekit } from '@sveltejs/kit/vite';
import autoImport from 'sveltekit-autoimport';
import { defineConfig } from 'vite';

export default defineConfig({
	plugins: [
    autoImport({
      components: ['./src/lib/components'],
    }),
    sveltekit()
  ],
  define: {
    'process.env': {}
  },
  server: {
    hmr: {
      clientPort: process.env.HMR_HOST ? 443 : 24678,
      host: process.env.HMR_HOST 
        ? process.env.HMR_HOST.substring("https://".length) 
        : "localhost"
    }
  }
});
