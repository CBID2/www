# Contributing to the Good Docs

We'd love your help.

* Have you noticed something that could be improved?
* Want to share your tech writing expertise?
* Do you have material you'd like to integrate into our baseline?
* Do you know of research which should be referenced?
* Something else?

Please do reach out to us with your ideas.

## Contact us

* Slack: [https://thegooddocs.slack.com/](https://thegooddocs.slack.com/)
* Email list: [https://groups.io/g/thegooddocsproject/](https://groups.io/g/thegooddocsproject/)

## About contributing

### Licenses

By contributing to this project we expect you to agree to the [Developer Certificate of Origin](https://developercertificate.org/) (DCO).
This document was created by the Linux Kernel community and is a simple statement that you, as a contributor, have the legal right to make the contribution, and agree to do so under our [terms of use](https://www.thegooddocsproject.dev/terms-of-use).

### Contributing

You can improve our website by submitting a merge request to our [GitLab website repo](https://gitlab.com/tgdp/wwww).
Here is [how](https://guides.github.com/activities/hello-world/).

It is best practice create a new issue and discuss your proposed changes before creating a new branch and submitting a merge request.

1. Create an issue in the [GitLab issue tracker](https://gitlab.com/tgdp/www/-/issues).
2. Be as specific as you can in the scope of work within the issue.
Be sure to include the "why" for considering the work.
3. Share the link to the issue on the #website channel in our [Slack instance](https://thegooddocs.slack.com) to get a review of your idea.
4. Once folks agree on your proposed changes you can begin work.

### Issue tracker

We track outstanding work in our [GitLab issue tracker](https://gitlab.com/tgdp/www/-/issues).
To keep our issue tracker manageable, we prefer you discuss suggestions or issues in the #website Slack channel before adding an issue to the tracker.

### Style guide

We use the [Google Style Guide](http://google.github.io/styleguide/) and American spelling, as per the [Merriam-Webster Online](https://www.merriam-webster.com/) dictionary.
If you're used to working with such reference books, that's great; if not, please contribute anyway, a tech writer will likely update your contribution later.
