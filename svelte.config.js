import adapter from '@sveltejs/adapter-static';
import preprocess from 'svelte-preprocess'
import Processor from '@asciidoctor/core'
const processor = Processor()

function svasciidoc() {
  return {
    markup: async ({ content, filename }) => {
      if (filename.endsWith('adoc')) {
        const html = processor.convert(content, {'standalone':false, 'safe':'unsafe', 'attributes':{'icons':'font', 'allow-uri-read':'', 'sectanchors':true, 'skip-front-matter':true, 'imagesdir':'https://tgdp-assets.imgix.net'}})
        return {
          code: html
        };
      }
    }
  }
};

/** @type {import('@sveltejs/kit').Config} */
const config = {
  extensions: ['.svelte', '.adoc'],
	kit: {
		// adapter-auto only supports some environments, see https://kit.svelte.dev/docs/adapter-auto for a list.
		// If your environment is not supported or you settled on a specific environment, switch out the adapter.
		// See https://kit.svelte.dev/docs/adapters for more information about adapters.
		adapter: adapter({
			// default options are shown. On some platforms
			// these options are set automatically - see below
			pages:'build',
			assets:'build',
			fallback: undefined,
			precompress: false,
			strict: true
		}),
    prerender: {
      handleMissingId:'warn',
      handleHttpError:'warn'
    }
	},
  preprocess: [
    preprocess(),
    svasciidoc()
  ]
};

export default config;
